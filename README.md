Simple **SOAP** web service for searching an account in DB using Top-Down model  
WSDL location: `http://localhost:8080/ws/search-service.wsdl`  
Technology used: Spring Boot, Spring WS, Spring Data, Postgres  
__  
Written by **_Yakovlev Alexander_**  
