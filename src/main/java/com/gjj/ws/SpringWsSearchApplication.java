package com.gjj.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWsSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWsSearchApplication.class, args);
	}
}
