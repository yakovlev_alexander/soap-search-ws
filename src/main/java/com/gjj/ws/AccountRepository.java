package com.gjj.ws;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepository extends CrudRepository<PersistentAccount, Long> {

    List<PersistentAccount> findByNameContainingIgnoreCaseOrFamilyNameContainingIgnoreCase(String namePattern, String familyNamePattern);
}
