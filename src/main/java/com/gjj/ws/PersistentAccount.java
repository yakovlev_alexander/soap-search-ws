package com.gjj.ws;

import javax.persistence.*;

@Entity
@Table(name = "account")
public class PersistentAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(name = "FAMILY_NAME", nullable = false)
    private String familyName;

    private Integer age;

    public PersistentAccount() {
    }

    public PersistentAccount(String name, String familyName) {
        this.name = name;
        this.familyName = familyName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Integer getAge() {
        return age != null ? age : 0;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "PersistentAccount{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", familyName='" + familyName + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersistentAccount that = (PersistentAccount) o;

        if (!name.equals(that.name)) return false;
        return familyName.equals(that.familyName);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + familyName.hashCode();
        return result;
    }
}
