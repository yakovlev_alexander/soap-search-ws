package com.gjj.ws;

import localhost.soap_search.Account;
import localhost.soap_search.SearchRequest;
import localhost.soap_search.SearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
public class SearchEndpoint {
    private static final String NAMESPACE_URI = "http://localhost/soap_search";

    private final AccountRepository repository;

    @Autowired
    public SearchEndpoint(AccountRepository repository) {
        this.repository = repository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "searchRequest")
    @ResponsePayload
    public SearchResponse searchAccounts(@RequestPayload SearchRequest searchRequest) {
        List<PersistentAccount> accounts = repository.findByNameContainingIgnoreCaseOrFamilyNameContainingIgnoreCase(searchRequest.getPattern(),searchRequest.getPattern());
        SearchResponse response = new SearchResponse();
        List<Account> responseAccounts = response.getAccounts();
        accounts.forEach(account -> responseAccounts.add(toResponseAccount(account)));
        return response;
    }

    private static Account toResponseAccount(PersistentAccount account) {
        Account responseAccount = new Account();
        responseAccount.setName(account.getName());
        responseAccount.setFamilyName(account.getFamilyName());
        responseAccount.setAge(account.getAge());
        return responseAccount;
    }
}
