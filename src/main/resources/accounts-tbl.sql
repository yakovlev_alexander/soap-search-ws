CREATE TABLE IF NOT EXISTS "account"
(
  ID BIGSERIAL PRIMARY KEY,
  NAME VARCHAR(255) NOT NULL,
  FAMILY_NAME VARCHAR(255) NOT NULL,
  AGE INT2
);
INSERT INTO "account"(name, family_name, age) VALUES('Alexander', 'Yakovlev', 26);
INSERT INTO "account"(name, family_name) VALUES('Emin', 'Mamedov');
INSERT INTO "account"(name, family_name) VALUES('Vitaly', 'Severin');