package com.gjj.ws;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringWsSearchApplicationTests {

	@Autowired
	private AccountRepository repository;

	@Test
	public void searchTest_ov() {
		List<PersistentAccount> accounts = repository.findByNameContainingIgnoreCaseOrFamilyNameContainingIgnoreCase("ov", "ov");
		assertTrue(accounts.contains(getAccounts().get(0)));
		assertTrue(accounts.contains(getAccounts().get(1)));
		assertFalse(accounts.contains(getAccounts().get(2)));
	}

	@Test
	public void searchTest_em() {
		List<PersistentAccount> accounts = repository.findByNameContainingIgnoreCaseOrFamilyNameContainingIgnoreCase("em", "em");
		assertFalse(accounts.contains(getAccounts().get(0)));
		assertTrue(accounts.contains(getAccounts().get(1)));
		assertFalse(accounts.contains(getAccounts().get(2)));
	}

	@Test
	public void searchTest_ev() {
		List<PersistentAccount> accounts = repository.findByNameContainingIgnoreCaseOrFamilyNameContainingIgnoreCase("ev", "ev");
		assertTrue(accounts.contains(getAccounts().get(0)));
		assertFalse(accounts.contains(getAccounts().get(1)));
		assertTrue(accounts.contains(getAccounts().get(2)));
	}

	private List<PersistentAccount> getAccounts() {
		List<PersistentAccount> acc = new LinkedList<>();
		acc.add(new PersistentAccount("Alexander", "Yakovlev"));
		acc.add(new PersistentAccount("Emin", "Mamedov"));
		acc.add(new PersistentAccount("Vitaly", "Severin"));
		return acc;
	}

}
